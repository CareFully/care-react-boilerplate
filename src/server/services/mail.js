/* eslint-disable class-methods-use-this */
import sgMail from '@sendgrid/mail';

class Mail {
    constructor() {
        sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    }

    send(to, messageHtml, messageText, subject = process.env.APP_NAME) {
        const msg = {
            to: to,
            from: process.env.SENDGRID_EMAIL,
            subject: subject,
            text: messageText,
            html: messageHtml
        };
        return sgMail.send(msg);
    }

    sendTemplate(to, templateId, data = {}, subject = process.env.APP_NAME) {
        const msg = {
            to: to,
            from: process.env.SENDGRID_EMAIL,
            subject: subject,
            templateId: templateId,
            dynamic_template_data: {...data, subject: subject}
        };
        return sgMail.send(msg);
    }
}

export default new Mail();
