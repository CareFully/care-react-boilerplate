/* eslint-disable newline-per-chained-call */
import {body, param} from 'express-validator';
import Mongoose from 'mongoose';
import User from '../models/user';

const USER_ID = () => param('userId').isMongoId();
const USER_NAME = () => [
    body('firstname', 'First name is required').isString().isLength({min: 1}),
    body('lastname', 'Last name is required').isString().isLength({min: 1})
];
const EMAIL = () => body('email', 'Email is required')
    .isString()
    .isEmail().withMessage('Invalid email')
    .normalizeEmail();

export default {
    user: [
        ...USER_NAME(),
        EMAIL().custom(async (value) => {
            const user = await User.findOne({email: value});
            if (user) {
                throw new Error('Email already in use');
            }
            return true;
        }),
        body('password', 'Password is required')
            .isString()
            .isLength({min: 6}).withMessage('Password must be at least 6 characters long')
    ],
    userUpdate: [
        USER_ID(),
        ...USER_NAME(),
        EMAIL().custom(async (value, {req}) => {
            const user = await User.findOne({
                _id: {$ne: Mongoose.Types.ObjectId(req.params.userId)},
                email: value
            });
            if (user) {
                throw new Error('Email already in use');
            }
            return true;
        })
    ],
    userId: [
        USER_ID
    ]
};
