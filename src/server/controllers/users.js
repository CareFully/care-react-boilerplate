/* eslint-disable class-methods-use-this */
/* eslint-disable no-underscore-dangle */
import ModelController from './model';
import User from '../models/user';

class UsersController extends ModelController {
    constructor() {
        super(User);
    }

    async getAll(req, res) {
        res.json(await User.find({}, {oldPasswords: 0, password: 0}));
    }

    async get(req, res) {
        if (!res.locals.user.admin && res.locals.user.userId !== req.params.userId) {
            return res.status(401).json({message: 'Insufficient permissions'});
        }
        return res.json(
            await User.findOne({_id: req.params.userId}, {oldPasswords: 0, password: 0})
        );
    }

    async update(req, res) {
        if (!res.locals.user.admin && res.locals.user.userId !== req.params.userId) {
            return res.status(401).json({message: 'Insufficient permissions'});
        }
        const user = await User.findById(req.params.userId);
        user.firstname = req.body.firstname;
        user.lastname = req.body.lastname;
        user.email = req.body.email;
        await user.save();
        return res.json({
            message: 'User updated',
            user: user
        });
    }

    async create(req, res) {
        try {
            const doc = new User({
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                email: req.body.email,
                password: {hash: req.body.password}
            });
            await doc.save();
            res.status(201).json({
                message: 'User created'
            });
        } catch (err) {
            res.status(400).json({
                message: err.message
            });
        }
    }

    async delete(req, res) {
        if (!res.locals.user.admin && res.locals.user.userId !== req.params.userId) {
            return res.status(401).json({message: 'Insufficient permissions'});
        }
        await this.Model.findByIdAndRemove(req.params.userId);
        return res.json({
            message: 'User deleted'
        });
    }
}

export default new UsersController();
