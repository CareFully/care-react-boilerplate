/* eslint-disable class-methods-use-this */
import jwt from 'jsonwebtoken';
import User from '../models/user';
import Mail from '../services/mail';
import Password from '../services/password';

class AuthController {
    async login(req, res) {
        const user = await User.findOne({email: req.body.email});
        if (user && Password.verify(user, req.body.password)) {
            const token = this.createSessionToken(user);
            res.json({message: 'Logged in successfully', token: token, user: user});
        } else {
            res.status(400).json({message: 'Invalid email or password'});
        }
    }

    async resetPassword(req, res) {
        const user = await User.findOne({email: req.body.email});
        if (!user) {
            res.status(404).json({message: 'Did not find a use with given email'});
            return;
        }
        const token = jwt.sign({userId: user._id, key: user.password._id}, process.env.RESET_JWT_SECRET, {expiresIn: '30m'});
        Mail.sendTemplate(user.email, process.env.SENDGRID_TEMPLATE_RESET_PASSWORD, {
            firstname: user.firstname,
            resetlink: `${process.env.APP_HOST}/new-password?token=${token}`
        }, 'Reset your password');
        res.json({message: 'Reset password email sent'});
    }

    async newPassword(req, res) {
        try {
            const decoded = jwt.verify(req.body.token, process.env.RESET_JWT_SECRET);
            const user = await User.findOne({_id: decoded.userId, 'password._id': decoded.key});
            if (!user) {
                res.status(401).json({message: 'Token expired'});
                return;
            }
            if (await Password.isUsedPassword(user, req.body.password)) {
                res.status(400).json({message: 'You have already used entered password before'});
                return;
            }
            user.password.remove();
            user.password = {hash: req.body.password};
            await user.save();
            res.status(200).json({message: 'Password updated'});
        } catch (err) {
            res.status(401).json({message: 'Token invalid'});
        }
    }

    async token(req, res) {
        const user = await User.findById(res.locals.user.userId);
        if (user) {
            const token = this.createSessionToken(user);
            res.json({message: 'Token updated', token: token, user: user});
        } else {
            res.status(404).json({message: 'User not found'});
        }
    }

    createSessionToken(user) {
        return jwt.sign({userId: user._id, admin: user.admin, key: user.password._id}, process.env.JWT_SECRET, {expiresIn: '15d'});
    }
}

export default new AuthController();
