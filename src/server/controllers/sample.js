/* eslint-disable class-methods-use-this */

class SampleController {
    async getFoo(req, res) {
        res.json({foo: 'bar'});
    }
}

export default new SampleController();
