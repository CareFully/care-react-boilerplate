/* eslint-disable no-underscore-dangle */
import express from 'express';
import userRoutes from './users';
import authRoutes from './auth';
import sampleRoutes from './sample';

const router = express.Router();

router.use('/foo', sampleRoutes);
router.use('/user', userRoutes);
router.use('/auth', authRoutes);

export default router;
