import express from 'express';
import validation from '../middleware/validation';
import authorization from '../middleware/authorization';
import AuthController from '../controllers/auth';
import authSchemas from '../schemas/auth';

const router = express.Router();

router.post('/login', validation(authSchemas.login), (req, res) => AuthController.login(req, res));
router.post('/reset-password', validation(authSchemas.email), (req, res) => AuthController.resetPassword(req, res));
router.post('/new-password', validation(authSchemas.newPassword), (req, res) => AuthController.newPassword(req, res));
router.get('/token', authorization(), (req, res) => AuthController.token(req, res));

export default router;
