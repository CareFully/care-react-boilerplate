import express from 'express';
import validation from '../middleware/validation';
import authorization from '../middleware/authorization';
import userSchemas from '../schemas/users';
import UsersController from '../controllers/users';

const router = express.Router();

router.get('/', (req, res) => UsersController.getAll(req, res));
router.post('/', validation(userSchemas.user), (req, res) => UsersController.create(req, res));
router.delete('/', authorization(true), (req, res) => UsersController.deleteAll(req, res));

router.get('/:userId', authorization(), validation(userSchemas.userId), (req, res) => UsersController.get(req, res));
router.put('/:userId', authorization(), validation(userSchemas.userUpdate), (req, res) => UsersController.update(req, res));
router.delete('/:userId', authorization(), validation(userSchemas.userId), (req, res) => UsersController.delete(req, res));

export default router;
