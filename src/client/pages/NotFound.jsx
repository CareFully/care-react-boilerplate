/* eslint-disable quote-props */
import React from 'react';
import PropTypes from 'prop-types';
import {Link as RouterLink} from 'react-router-dom';
import {Typography, Link} from '@material-ui/core';
import {connect} from 'react-redux';
import PageContainer from '../components/PageContainer';
import {setTitle} from '../actions/main';

class NotFound extends React.Component {
    static propTypes = {
        setTitle: PropTypes.func.isRequired
    };

    componentDidMount() {
        this.props.setTitle('Page not found');
    }

    render() {
        return (
            <PageContainer>
                <Typography variant="h5" component="h1" gutterBottom>
                    Nothing found here :(
                </Typography>
                <Typography variant="body1" gutterBottom>
                    This page does not contain anything useful.
                </Typography>
                <Link component={RouterLink} to="/">
                    Go back home
                </Link>
            </PageContainer>
        );
    }
}

const actionCreators = {
    setTitle
};

export default connect(null, actionCreators)(NotFound);
