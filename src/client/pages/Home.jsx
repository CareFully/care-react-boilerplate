import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Typography} from '@material-ui/core';
import {isFooLoading, getFoo} from '../selectors';
import {loadFoo, setTitle} from '../actions/main';
import PageContainer from '../components/PageContainer';

class Home extends React.Component {
    static propTypes = {
        foo: PropTypes.string.isRequired,
        loading: PropTypes.bool.isRequired,

        setTitle: PropTypes.func.isRequired,
        loadFoo: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        this.props.loadFoo();
        this.props.setTitle('Home');
    }

    render() {
        return (
            <PageContainer>
                <Typography variant="h5" component="h1" gutterBottom>
                    Welcome to care-react-boilerplate
                </Typography>
                <Typography variant="body1">
                    {`Foo: ${this.props.loading ? 'Loading...' : this.props.foo}`}
                </Typography>
            </PageContainer>
        );
    }
}

const mapStateToProps = (state) => ({
    foo: getFoo(state),
    loading: isFooLoading(state)
});

const actionCreators = {
    setTitle,
    loadFoo
};

export default connect(mapStateToProps, actionCreators)(Home);
