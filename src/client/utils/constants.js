/* eslint-disable object-curly-newline */

export const DRAWER_WIDTH = 240;

export const MENU_ITEMS = [
    {route: '/', name: 'Home', icon: 'home'},
    {route: '/account', name: 'Account', icon: 'account_circle', protected: true}
];
