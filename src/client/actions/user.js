/* eslint-disable prefer-destructuring */
import {push} from 'connected-react-router';
import {error, success} from 'react-toastify-redux';
import requestAction from '../utils/requestAction';
import {SET_USER} from './auth';

export const FETCH_REGISTER = 'FETCH_REGISTER';
export const FETCH_UPDATE = 'FETCH_UPDATE';

function onRegister() {
    return (dispatch) => {
        dispatch(push('/login'));
        dispatch(success('Account created, please log in to continue'));
    };
}

function onRegisterFailed(response) {
    return (dispatch) => {
        dispatch(error(response.message));
    };
}

export function loadRegister(firstname, lastname, email, password) {
    return (dispatch) => {
        dispatch(requestAction({
            url: '/api/user',
            method: 'POST',
            data: {
                firstname: firstname,
                lastname: lastname,
                email: email,
                password: password
            },
            onSuccess: onRegister,
            onFailure: onRegisterFailed,
            label: FETCH_REGISTER
        }));
    };
}

function onUpdate(response) {
    return (dispatch) => {
        dispatch(success('Account settings updated'));
        dispatch({type: SET_USER, user: response.user});
    };
}

function onUpdateFailed(response) {
    return (dispatch) => {
        dispatch(error(response.message));
    };
}

export function loadUpdate(id, firstname, lastname, email) {
    return (dispatch) => {
        dispatch(requestAction({
            url: `/api/user/${id}`,
            method: 'PUT',
            data: {
                firstname: firstname,
                lastname: lastname,
                email: email
            },
            onSuccess: onUpdate,
            onFailure: onUpdateFailed,
            label: FETCH_UPDATE
        }));
    };
}
