import {createStore, applyMiddleware, compose} from 'redux';
import {persistStore} from 'redux-persist';
import thunk from 'redux-thunk';
import {createBrowserHistory} from 'history';
import {routerMiddleware} from 'connected-react-router';
import createRootReducer from '../reducers/index';
import requestsMiddleware from '../middleware/requestsMiddleware';

// eslint-disable-next-line no-underscore-dangle
const storeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const history = createBrowserHistory();
const rootReducer = createRootReducer(history);

export default function initStore() {
    const store = createStore(
        rootReducer,
        storeEnhancers(applyMiddleware(
            routerMiddleware(history),
            thunk,
            requestsMiddleware
        ))
    );
    return store;
}

export function initPersistor(store) {
    const persistor = persistStore(store);
    return persistor;
}
