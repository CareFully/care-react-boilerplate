import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'connected-react-router';
import {PersistGate} from 'redux-persist/integration/react';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import initStore, {initPersistor, history} from './store/index';
import App from './components/App';

const root = document.getElementById('app');
const store = initStore();
const persistor = initPersistor(store);
const baseTheme = createMuiTheme();
const theme = createMuiTheme({
    ...baseTheme,
    overrides: {

    }
});

render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <ConnectedRouter history={history}>
                <ThemeProvider theme={theme}>
                    <App />
                </ThemeProvider>
            </ConnectedRouter>
        </PersistGate>
    </Provider>,
    root
);
