import {combineReducers} from 'redux';
import {connectRouter} from 'connected-react-router';
import {toastsReducer} from 'react-toastify-redux';
import storage from 'redux-persist/lib/storage';
import sessionStorage from 'redux-persist/lib/storage/session';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import {persistReducer} from 'redux-persist';
import mainReducer from './main';
import requestsReducer from './requests';
import authReducer from './auth';
import AuthTransform from '../transforms/auth';

const persistConfig = {
    key: 'root',
    stateReconciler: autoMergeLevel2,
    transforms: [AuthTransform],
    storage: storage,
    whitelist: ['auth']
};

const sessionPersistConfig = {
    key: 'auth',
    storage: sessionStorage,
    whitelist: ['user', 'token']
};

const createRootReducer = (history) => {
    const rootReducer = combineReducers({
        router: connectRouter(history),
        toasts: toastsReducer,
        requests: requestsReducer,
        auth: persistReducer(sessionPersistConfig, authReducer),
        main: mainReducer
    });
    return persistReducer(persistConfig, rootReducer);
};

export default createRootReducer;
