import produce from 'immer';
import createReducer from '../utils/createReducer';
import {SET_FOO, SET_TITLE} from '../actions/main';

const initialState = {
    title: '',
    foo: ''
};

const setFoo = produce((draft, action) => {
    draft.foo = action.response.foo;
});

const setTitle = produce((draft, action) => {
    draft.title = action.title;
});

export default createReducer(initialState, {
    [SET_TITLE]: setTitle,
    [SET_FOO]: setFoo
});
