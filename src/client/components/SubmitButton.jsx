import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import {Button} from '@material-ui/core';

class SubmitButton extends React.Component {
    static propTypes = {
        classes: PropTypes.object.isRequired,
        title: PropTypes.string.isRequired,
        loading: PropTypes.bool.isRequired
    };

    render() {
        const {
            loading, title, classes
        } = this.props;
        return (
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.root}
                disabled={loading}
            >
                {title}
            </Button>
        );
    }
}

export default withStyles((theme) => ({
    root: {
        margin: theme.spacing(1, 0, 2)
    }
}))(SubmitButton);
