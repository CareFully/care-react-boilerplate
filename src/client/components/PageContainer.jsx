import React from 'react';
import {Container} from '@material-ui/core';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';

class PageContainer extends React.Component {
    static propTypes = {
        classes: PropTypes.object.isRequired,
        children: PropTypes.node.isRequired
    };

    render() {
        const {classes, children} = this.props;
        return (
            <Container className={classes.container}>
                {children}
            </Container>
        );
    }
}

export default withStyles((theme) => ({
    container: {
        marginTop: theme.spacing(4)
    }
}))(PageContainer);
