import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import {Container, Typography} from '@material-ui/core';

class FormContainer extends React.Component {
    static propTypes = {
        classes: PropTypes.object.isRequired,
        children: PropTypes.node.isRequired,
        title: PropTypes.string.isRequired,
        onSubmit: PropTypes.func
    };

    static defaultProps = {
        onSubmit: null
    }

    render() {
        const {
            classes, title, children, onSubmit
        } = this.props;
        return (
            <Container maxWidth="sm" className={classes.container}>
                <Typography component="h1" variant="h4">
                    {title}
                </Typography>
                {
                    onSubmit
                        ? (
                            <form className={classes.form} noValidate onSubmit={onSubmit}>
                                {children}
                            </form>
                        )
                        : <>{children}</>
                }
            </Container>
        );
    }
}

export default withStyles((theme) => ({
    container: {
        marginTop: theme.spacing(4)
    },
    form: {
        width: '100%', // Fix IE 11 issue, apparently.
        marginTop: theme.spacing(4)
    }
}))(FormContainer);
