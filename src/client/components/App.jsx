import React from 'react';
import {Route, Switch} from 'react-router-dom';
import {ToastContainer} from 'react-toastify-redux';
import {Slide} from 'react-toastify';
import CssBaseline from '@material-ui/core/CssBaseline';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core';
import {compose} from 'redux';
import {connect} from 'react-redux';
import Home from '../pages/Home';
import Login from '../pages/Login';
import NotFound from '../pages/NotFound';
import Header from './Header';
import 'react-toastify/dist/ReactToastify.css';
import '../assets/scss/style.scss';
import Register from '../pages/Register';
import ResetPassword from '../pages/ResetPassword';
import NewPassword from '../pages/NewPassword';
import withUser from '../hocs/withUser';
import {loadToken} from '../actions/auth';
import Account from '../pages/Account';
import UserRoute from './UserRoute';

class App extends React.Component {
    static propTypes = {
        classes: PropTypes.object.isRequired,

        isLoggedIn: PropTypes.bool.isRequired,
        loadToken: PropTypes.func.isRequired
    };

    componentDidMount() {
        if (this.props.isLoggedIn) {
            this.props.loadToken();
        }
    }

    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <CssBaseline />
                <Header />
                <ToastContainer transition={Slide} className={classes.toastContainer} />
                <div className={classes.content}>
                    <div className={classes.toolbar} />
                    <Switch>
                        <Route path="/login" exact component={Login} />
                        <Route path="/register" exact component={Register} />
                        <Route path="/reset-password" exact component={ResetPassword} />
                        <Route path="/new-password" exact component={NewPassword} />
                        <Route path="/" exact component={Home} />
                        <UserRoute path="/account" exact component={Account} />
                        <Route path="*" component={NotFound} />
                    </Switch>
                </div>
            </div>
        );
    }
}

const actionCreators = {
    loadToken
};

export default withUser(compose(
    withStyles((theme) => ({
        root: {
            display: 'flex'
        },
        toastContainer: {
            marginTop: '5em'
        },
        toolbar: theme.mixins.toolbar,
        content: {
            flexGrow: 1
        }
    })),
    connect(null, actionCreators)
)(App));
