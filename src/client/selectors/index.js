import qs from 'query-string';
import {FETCH_FOO} from '../actions/main';
import {FETCH_LOGIN, FETCH_RESET_PASSWORD, FETCH_NEW_PASSWORD} from '../actions/auth';
import {FETCH_REGISTER, FETCH_UPDATE} from '../actions/user';

function isRequestLoading(requestName, state) {
    const request = state.requests[requestName];
    if (request && request.loading) {
        return true;
    }
    return false;
}

function getRequestError(requestName, state) {
    const request = state.requests[requestName];
    if (request && request.error) {
        return request.error;
    }
    return false;
}

export const getQueryParams = (state) => qs.parse(state.router.location.search);
export const getPathname = (state) => state.router.location.pathname;

export const getTitle = (state) => state.main.title;
export const getFoo = (state) => state.main.foo;
export const getFooError = (state) => getRequestError(FETCH_FOO, state);
export const isFooLoading = (state) => isRequestLoading(FETCH_FOO, state);

export const getUser = (state) => state.auth.user;
export const getUserToken = (state) => state.auth.token;
export const getRememberMe = (state) => state.auth.rememberMe;
export const getLoginError = (state) => getRequestError(FETCH_LOGIN, state);
export const isLoginLoading = (state) => isRequestLoading(FETCH_LOGIN, state);
export const isNewPasswordLoading = (state) => isRequestLoading(FETCH_NEW_PASSWORD, state);
export const isResetPasswordLoading = (state) => isRequestLoading(FETCH_RESET_PASSWORD, state);

export const getRegisterError = (state) => getRequestError(FETCH_REGISTER, state);
export const isRegisterLoading = (state) => isRequestLoading(FETCH_REGISTER, state);

export const getUpdateError = (state) => getRequestError(FETCH_UPDATE, state);
export const isUpdateLoading = (state) => isRequestLoading(FETCH_UPDATE, state);
